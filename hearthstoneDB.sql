/*
	Here's the basic sql code for our database
	https://docs.oracle.com/cd/B28359_01/server.111/b28318/datatype.htm

	here's a link to the oracle data types and their specifications
	how to use them and what not.
*/
SPOOL project.out
SET ECHO ON

/*
    CIS 353 - Database Design Project
    Benjamin J. Benson
    Vincent Ball
    Kristian
    Brandon Attala
*/



CREATE TABLE player (
	email char(30) PRIMARY KEY,
	username char(27),
	password char(15),
	friendswith char(27),
        numfriends number(*,0),
        -- PRIMARY KEY(email, username),
	CONSTRAINT chk_email CHECK(email LIKE '%@%'),
	CONSTRAINT chk_numfriends CHECK(numfriends >= 0)
);

CREATE TABLE card (
    cardname char(20) PRIMARY KEY,
    dust number(*,0),
    health number(*,0) NOT NULL,
    manacost number(*, 0) NOT NULL,
    attack number(*, 0) NOT NULL,
    foil char(9) NOT NULL,
    rarity char(9) NOT NULL,
    ptype char(10) NOT NULL,
    CONSTRAINT card_attack_health CHECK(health > 0 OR attack > 0),
    CONSTRAINT card_manacost CHECK(manacost > 0),
    CONSTRAINT check_rarity CHECK(rarity IN('common','rare','epic','legendary')),
    CONSTRAINT is_foil CHECK(foil IN('true','false'))
);

CREATE TABLE hero (
	name char(15) PRIMARY KEY,
	heropower char(25) NOT NULL,
	description char(30) NOT NULL,
	CONSTRAINT hero_name CHECK (name IN('Druid','Hunter','Priest', 'Rogue','Shaman', 'Warlock'))
);

CREATE TABLE pack (
	pemail char(30),
        ptype char(15) PRIMARY KEY,
	count number(*,0),
	CONSTRAINT pack_contains CHECK (ptype IN('Classic','GvG','GoOG')),
	CONSTRAINT no_packs FOREIGN KEY (pemail) REFERENCES player(email)
		ON DELETE CASCADE
	--
	-- Add a constraint that  the pemail is a foreign key to player email
	-- and on delete cascade so that when a player 
	-- deletes his account it deletes the packs he or she owns
);

CREATE TABLE quests (
	pemail char(30),
	qname char(20) PRIMARY KEY,
	description char(15) NOT NULL, 
	gold number(*, 0),
	CONSTRAINT quest_gold CHECK (gold > 0),
	CONSTRAINT no_quests FOREIGN KEY (pemail) REFERENCES player(email)
                ON DELETE CASCADE
	-- edited this entity so that qname is a 
	-- key rather than the both of them be a key
	-- instead pemail is a weak key.
);

CREATE TABLE mechanics (
    mname char(10),
    cardname char(20),
    ability char(25),
    PRIMARY KEY(cardname, ability),
    CONSTRAINT del_card FOREIGN KEY (cardname) REFERENCES 
	card(cardname) ON DELETE CASCADE
);

CREATE TABLE decks (
	pemail char(30),
	deckname char(20),
	--- this next variable needs to be a foreign key for the hero table
	heroname char(25),
        PRIMARY KEY(pemail, deckname),
	CONSTRAINT no_decks FOREIGN KEY (pemail) REFERENCES player(email)
                ON DELETE CASCADE
);

CREATE TABLE contains(
	cardname char(25),
	dname char(25),
	copies number(*,0),
        PRIMARY KEY(cardname, dname) 
);

CREATE TABLE playerlanguage (
	pemail char(30),
	planguage char(10),
        PRIMARY KEY(pemail, planguage),
	 CONSTRAINT no_language FOREIGN KEY (pemail) REFERENCES player(email)
                ON DELETE CASCADE
);

SET FEEDBACK OFF
/*
    populate the sql table here
    insertion commands and such 
    use the SELECT * FROM table
    to print out each table name once there tables are populated

*/

-- the player table can have insertion values of 
--
insert into player values('benben@mail.gvsu','benjamin benson','password1','brandon attala',0);
insert into player values('brandonA@mail.edu','brandon attala','password2','vincent ball',5);
insert into player values('vincent@mail.gvsu.edu','vincent ball', 'password3', 'kreeestian',3);
insert into player values('kristian@mail.gvsu.edu','kreeestian', 'password4','benjamin benson',5);

-- card values are as follows 
-- cardname, health, manacost, attack, foil, rarity, packtype
insert into card values('molten giant', 100, 8, 20, 8, 'true', 'epic','basic');
insert into card values('clockwork giant', 100, 8, 12,8,'false', 'epic', 'GVM');
insert into card values('mountain giant', 100, 8, 12, 8, 'false', 'epic', 'GVM');

-- hero table are as follows
-- name, heropower, description
insert into hero values('Druid', 'fireball', 'malfurion stormrage');
insert into hero values('Hunter', 'shoot', 'Rexxar or alleria windrunner');
insert into hero values('Priest', 'andoin wrynn', 'heal');
insert into hero values('Rogue', 'Valeera Sanguinar', 'cheap shot');
insert into hero values('Shaman', 'thrall', 'totem');
insert into hero values('Warlock', 'gul dan', 'bleed');

-- insert into pack type
-- ptype and pemail
-- it's a simulation of pack types
insert into pack values('benben@mail.gvsu','Classic',2);
insert into pack values('benben@mail.gvsu','GvG',3);
insert into pack values('benben@mail.gvsu','GoOG',4);
insert into pack values('vincent@mail.gvsu.edu','GvG',6);

-- insert into the quest table
-- pemail, quest name, description, gold awarded
insert into quests values('benben@mail.gvsu','mage','domination',30);
insert into quests values('benben@mail.gvsu', 'hunter', 'domination',60);
insert into quests values('benben@mail.gvsu', 'druid', 'rampage',30);

-- insert into the mechanics table
-- mechanic name, cardname (foreign key), ability
insert into mechanics values('charge', 'molten giant','no summoning sickness');
insert into mechanics values('reduce', 'clockwork giant','reduce spell cost');
insert into mechanics values('jank','mountain giant', 'it just works');

-- insert into decks table
-- player email, deckname, heroname
insert into decks values('benben@mail.gvsu','destroyer','Druid');
insert into decks values('brandonA@mail.edu','mage deck','Mage');
insert into decks values('kristian@mail.gvsu.edu','face hunter','Hunter');
insert into decks values('vincent@mail.gvsu.edu','vince meat','Rogue');

-- insert into what deck and who owns that deck
-- ben owns 
insert into contains values('molten giant','destroyer',2);
insert into contains values('clockwork giant','destroyer',2);
insert into contains values('mountain giant','destroyer',2);

insert into playerlanguage values('benben@mail.gvsu','english');
insert into playerlanguage values('benben@mail.gvsu','spanish');
insert into playerlanguage values('benben@mail.gvsu','polish');


SELECT * from player;
SElECT * from card;
SELECT * from hero;
SELECT * from pack;
SELECT * from quests;
SELECT * from decks;
SELECT * from contains;
SELECT * from playerlanguage;

SET FEEDBACK ON
COMMIT
/*
	insert the queries here
*/

-- demonstration of a self join
-- find every player  who has a friend that knows english
SELECT P1.username, P2.friendswith, L.planguage 
FROM  player P1, player P2, playerlanguage L
WHERE P1.friendswith = P2.username AND
      P1.email = L.pemail AND
      L.planguage = 'english'
order by P1.username;

-- demonstration of a 4 table join
--  find every player, who has friend that owns a deck with a hero name of rogue
SELECT P1.username, P2.friendswith, D.deckname, H.name
FROM player P1, player P2, hero H, decks D
WHERE P1.friendswith = P2.username AND
      P2.email = D.pemail  AND
      D.heroname = H.name AND
      H.name = 'Rogue';

/*
Find all users who have more than 2 friends or have a quest with gold greater than 50
UNION
*/
Select P.email, P.username
From Player P
Where numfriends > 2
UNION
Select P.email, P.username
From Player P, Quests Q
Where P.email = Q.pemail AND Q.gold > 50;

/*
Find all users who have more than 2 friends AND have a heroname of Rogue
INTERSECT
*/
Select P.email, P.username
From Player P
Where numfriends > 2
INTERSECT
Select P.email, P.username
From Player P, Decks D
Where P.email = D.pemail AND D.heroname = 'Rogue';

/*
Find Max, Minimum, and average of manacost values
MAX, MIN, AVG
*/
SELECT MAX(manacost) AS maxManacost, AVG(manacost) AS averageManacost, MIN(manacost) as minManacost
FROM Card; 

-- a correlated subquery 
-- finds  anyone who has friends higher than the average number of friends
Select username, email, numfriends
from player
where numfriends > (
                    select AVG(numfriends)
                    from player 
                   );

-- Group by, having, order by
/*
	Finds the username of the player with more than one friend and
	order it by the highest number of friends to lowest.
*/
SELECT P.username, P.numfriends, COUNT(*)
FROM player P
WHERE P.numfriends > 1
GROUP BY P.username, P.numfriends
HAVING COUNT(*) > 0
ORDER BY P.numfriends DESC;

/*
SELECT P.email, COUNT (DISTINCT D.heroNAME) as hero_coverage, 
               COUNT (H.name) as hero_count  
FROM player P, decks D, hero H
WHERE P.email = D.playerEmail AND
               hero_coverage = hero_count
GROUP BY P.email, hero_coverage, hero_count;
*/


/*
  Section reserved for table constraints.
*/
-- Now the pack's table should only allow packs of a certain type.
-- As such we can only allow acks of a certain type into the table
INSERT INTO pack values ('ballenben@mail.gvsu','stuff',2);
INSERT INTO pack values('benben@mail.gvsu.edu','not',2);

-- Only allow gold awards to be above 0
insert into quests values('brandon@gvsu.edu', 'druid', 'derp',0);
insert into quests values('brand@gvsu.edu','druid','derp',-10);

-- only allow hero's of a certain name
insert into hero values('not mage','something','something');

-- check that a cards manacost is above 0
insert into card values('ghost', 1, 1, -1, 8, 'false', 'epic', 'GVM');
insert into card values('ghost',50,20,0,8,'false','epic','GVM');

--DELETE
DELETE FROM player WHERE email = 'vincent@mail.gvsu.edu'; 
--DECK for this email will be removed
SELECT * 
FROM decks;
--PACK for this email will be removed
SELECT * 
FROM pack;
--DELETE
DELETE FROM player WHERE email = 'benben@mail.gvsu'; 
--Quest table should now be empty
SELECT * 
FROM quests;
